// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');

mongoose.connect('mongodb://localhost/restTest'); // connect to our database

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.route('/data')
    // create a d (accessed at POST http://localhost:8080/api/data)
    .post(function(req, res) {
        var d = new d();      // create a new instance of the d model
        d.name = req.body.name;  // set the data name (comes from the request)

        // save the d and check for errors
        d.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'd created!' });
        });
    });

router.route('/data')
    .get(function(req, res) {
        d.find(function(err, data) {
            if (err)
                res.send(err);

            res.json(data);
        });
    });


router.route('/data/:d_id')
    // get the d with that id (accessed at GET http://localhost:8080/api/data/:d_id)
    .get(function(req, res) {
        d.findById(req.params.d_id, function(err, d) {
            if (err)
                res.send(err);
            res.json(d);
        });
    });

router.route('/data/:d_id')
    // update the d with this id (accessed at PUT http://localhost:8080/api/data/:d_id)
    .put(function(req, res) {
        // use our d model to find the d we want
        d.findById(req.params.d_id, function(err, d) {
            if (err)
                res.send(err);
            d.name = req.body.name;  // update the data info
            // save the d
            d.save(function(err) {
                if (err)
                    res.send(err);
                res.json({ message: 'd updated!' });
            });
        });
    });



// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);


var d     = require('./app/models/d');